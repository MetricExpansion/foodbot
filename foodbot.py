import discord
import asyncio
import dataset
import datetime
import dateparser
import random
import os

try:
    token = os.environ['DISCORD_TOKEN']
except ValueError:
    print("You need to set the DISCORD_TOKEN with your Discord bot API token.")
client = discord.Client()
if not os.path.exists('data'):
    print("Warning: Had to create storage directory.")
    os.makedirs('data')
db = dataset.connect('sqlite:///data/mydatabase.db')
meal_table = db['foodbot_meals']
meal_waiting_table = db['foodbot_waiting_meals']
user_table = db['foodbot_users']

code_lookup = ['Green', 'Meat']


print(list(meal_table.all()))
print(list(meal_waiting_table.all()))


@client.event
async def on_ready():
    print('Foodbot Logged in as')
    print(client.user.name)
    print(client.user.id)
    print('------')

async def on_message_raaa(message):
    if message.content.startswith('!test'):
        counter = 0
        tmp = await client.send_message(message.channel, 'Calculating messages...')
        async for log in client.logs_from(message.channel, limit=100):
            if log.author == message.author:
                counter += 1

        await client.edit_message(tmp, 'You have {} messages.'.format(counter))
    elif message.content.startswith('!sleep'):
        await asyncio.sleep(5)
        await client.send_message(message.channel, 'Done sleeping')

@client.event
async def on_message(message):
    sender = message.author
    if message.content == "Register":
        if not user_table.find_one(uid=sender.id):
            user_table.insert({'uid': sender.id, 'shortname': sender.name, 'nextproc': datetime.datetime.now()})
            await client.send_message(sender, "Welcome to Foodbot, {}! You are now registered.".format(sender.name))
        else:
            await client.send_message(sender, "Hi {}. You are already registered.".format(sender.name))
    elif message.content.startswith("Bug me at"):
        tz_str = message.content.split("Bug me at")[1]
        parsed_time = dateparser.parse(tz_str)
        if parsed_time:
            await client.send_message(sender, "I will bug you at {}".format(parsed_time))
            user_table.update({'uid': sender.id, 'nextproc': parsed_time}, ['uid'])
        else:
            await client.send_message(sender, "I didn't understand the time")
    elif message.content.startswith("When?"):
        await client.send_message(sender, "I will bug you at {}".format(user_table.find_one(uid=sender.id)['nextproc']))
    elif message.content.startswith("I had "):
        had_food = message.content.split("I had ")[1]
        try:
            food_idx = code_lookup.index(had_food)
        except:
            await client.send_message(sender, "I didn't understand what you had!")
        waiting_meal = meal_waiting_table.find_one(uid=sender.id, got_reply = False)
        if waiting_meal:
            waiting_meal['reply_was'] = food_idx
            waiting_meal['got_reply'] = True
            meal_waiting_table.update(waiting_meal, ['id'])
            await client.send_message(sender, "Recorded your meal!")
        else:
            await client.send_message(sender, "I'm not sure what meal this was for... adding it anyway. :)")
            meal_waiting_table.insert(dict(uid=sender.id, time = datetime.datetime.now(), got_reply = True, reply_was = food_idx))
    elif message.content.startswith("Questions"):
        num_meals = len(list(meal_waiting_table.find(uid=sender.id, got_reply=False)))
        await client.send_message(sender, "I still want to know about {} meals.".format(num_meals))


async def process_user(user, user_obj):
    num_meals = len(list(meal_waiting_table.find(uid=user['uid'], got_reply = False)))
    if num_meals:
        await client.send_message(user_obj, "You didn't tell me about {} meals!".format(num_meals))
    for old_meal in meal_waiting_table.find(uid=user['uid']):
        print(old_meal)
        reply = old_meal['reply_was'] if old_meal['got_reply'] else 1
        meal_table.insert(dict(uid = user['uid'], time = old_meal['time'], meal = reply))
        meal_waiting_table.delete(id=old_meal['id'])
    for i in range(3):
        r = random.random()
        p = 0.4
        selection = 0 if r > p else 1
        meal_waiting_table.insert(dict(uid = user['uid'], time = datetime.datetime.now(), got_reply = False, reply_was = -1))
        await client.send_message(user_obj, "Meal {} should be {}.".format(i+1, code_lookup[selection]))
    return


async def process_users():
    await client.wait_until_ready()
    while not client.is_closed:

        for user in user_table.all():
            if user['nextproc'] < datetime.datetime.now():
                user_obj = await client.get_user_info(user['uid'])
                await client.send_message(user_obj, "I'm figuring out your meals...")
                user['nextproc'] += datetime.timedelta(days=1)
                user_table.update(user, ['uid'])
                await process_user(user, user_obj)

        await asyncio.sleep(5) # task runs every 60 seconds



client.loop.create_task(process_users())
# loop = asyncio.get_event_loop()
# loop.run_until_complete(client.start(token))
client.run(token)