# Foodbot

You can run Foodbot from docker using

    docker volume create foodbotstorage
    docker run -v foodbotstorage:/app/data -e DISCORD_TOKEN=$TOKEN metricexpansion/foodbot

where $TOKEN is your discord API token.